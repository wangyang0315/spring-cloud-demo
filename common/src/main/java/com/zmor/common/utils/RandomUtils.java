package com.zmor.common.utils;

import java.util.Random;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 15:00 2018/7/18.
 * @ Description：
 * @ Modified By：
 */
public class RandomUtils {
    public static String generateRandom(int size) {
        int i;
        int count = 0;
        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuilder code = new StringBuilder();
        Random r = new Random();
        while (count < size) {
            i = Math.abs(r.nextInt(str.length));
            if (i >= 0 && i < str.length) {
                code.append(str[i]);
                count++;
            }
        }
        return code.toString();
    }
}
