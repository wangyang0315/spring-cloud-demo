package com.zmor.common.utils;

import com.google.common.collect.Lists;
import com.zmor.common.vo.BaseTreeVO;

import java.util.List;
import java.util.Objects;

/**
 *  树形结构工具类
 * @author wangyang
 * @date 2018/8/8 14:01
 */
public class TreeUtils {


    /**
     * 创建树
     * @param data
     * @return
     */
    public static <T extends BaseTreeVO<T, ID>, ID>  List<T> createTree(List<T> data, ID baseId) {
        List<T> tree = Lists.newArrayList();
        for(T res : data){
            if(Objects.equals(res.getParentId(),baseId)){
                List<T> childs = getAllChilds(res.getId(), data);
                res.setChildren(childs);
                tree.add(res);
            }
        }
        return  tree;
    }

    /**
     * 递归获取所有子节点
     */
    private static <T extends BaseTreeVO<T, ID>, ID> List<T> getAllChilds(ID id, List<T> data) {
        List<T> childs = Lists.newArrayList();
        for(T res : data){
            ID parentId = res.getParentId();
            if(Objects.equals(id,parentId)){
                if (existChild(res.getId(), data)) {
                    List<T> childTwo = getAllChilds(res.getId(), data);
                    res.setChildren(childTwo);
                }
                childs.add(res);
            }
        }
        return childs;
    }

    /**
     *  判断是否存在子节点
    **/
    private static <T extends BaseTreeVO<T, ID>, ID> boolean existChild(ID id, List<T> list) {
        for(T res:list){
            if(Objects.equals(id,res.getParentId())){
                return true;
            }
        }
        return false;
    }

}
