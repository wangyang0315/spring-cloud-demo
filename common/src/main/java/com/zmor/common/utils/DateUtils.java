package com.zmor.common.utils;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

import static jdk.nashorn.internal.runtime.JSType.toLong;

/**
 * joda时间工具类
 * @author wangyang
 * @date 2018/7/30 15:29
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
public class DateUtils {

    private static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date longSecondsToDate(String dateTimeStr) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(STANDARD_FORMAT);
        DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

    public static Date longSecondsToDate(String dateTimeStr, String formatStr) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(formatStr);
        DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

    public static String dateToString(Date date){
        if(date == null){
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(STANDARD_FORMAT);
    }

    public static String dateToString(Date date,String formatStr){
        if(date == null){
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(formatStr);
    }

    /**
     * 秒数  转换成 date
     *
     * @param time 当前的秒数
     * @return date
     */
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    public static Date longSecondsToDate(Long time) {
        return new Date(toLong(time) * 1000L);
    }

}
