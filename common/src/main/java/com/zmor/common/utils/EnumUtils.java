package com.zmor.common.utils;


import com.zmor.common.enums.BaseStateEnumInterface;

import java.util.Arrays;

/**
 * @author wangyang
 * @date 2018/8/30 10:47
 */
public class EnumUtils {

    public static <E extends BaseStateEnumInterface> E getEnumByCode(int code, Class<E> clazz) {
        E[] values = clazz.getEnumConstants();
        return Arrays.stream(values).filter(e -> e.getState() == code).findFirst().get();
    }
}
