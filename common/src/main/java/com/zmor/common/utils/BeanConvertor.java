package com.zmor.common.utils;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;


/**
 * bean转化类
 *
 * @author wangyang
 */
@Slf4j
public class BeanConvertor {


    /**
     * 方法说明：对象转换
     *
     * @param source           原对象
     * @param target           目标对象
     * @param ignoreProperties 排除要copy的属性
     * @return
     */
    public static <T> T copy(Object source, Class<T> target, String... ignoreProperties) {
        T targetInstance;
        try {
            targetInstance = target.newInstance();
        } catch (Exception e) {
            log.error("{} 转换成对象 {} 失败",source.getClass().toString(),target.toString(),e);
            throw new RuntimeException(source.getClass().toString() + " 转换成对象" + target.toString() +"失败");
        }
        if (ArrayUtils.isEmpty(ignoreProperties)) {
            BeanUtils.copyProperties(source, targetInstance);
        } else {
            BeanUtils.copyProperties(source, targetInstance, ignoreProperties);
        }
        return targetInstance;

    }

    /**
     * 方法说明：对象转换(List)
     *
     * @param list             原对象
     * @param target           目标对象
     * @param ignoreProperties 排除要copy的属性
     * @return
     */
    public static <T, E> List<T> copyList(List<E> list, Class<T> target, String... ignoreProperties) {
        List<T> targetList = Lists.newArrayList();
        if (CollectionUtils.isEmpty(list)) {
            return targetList;
        }
        for (E e : list) {
            targetList.add(copy(e, target, ignoreProperties));
        }
        return targetList;
    }

    /**
     * 方法说明：map转化为对象
     *
     * @param map
     * @param t
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static <T> T mapToObject(Map<String, Object> map, Class<T> t) {
        T instance = null;
        try {
            instance = t.newInstance();
            if(map == null || map.size() == 0) {
                return instance;
            }
            org.apache.commons.beanutils.BeanUtils.populate(instance, map);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error("Map转换成对象 {} 失败",t.toString(),e);
            throw new RuntimeException("Map转换成对象 "+t.toString()+" 失败");
        }
        return instance;
    }

    /**
     * 方法说明：对象转化为Map
     *
     * @param object
     * @return
     */
    public static Map<?, ?> objectToMap(Object object) {
        if(object == null) {
            return null;
        }
        return new BeanMap(object);
    }
}