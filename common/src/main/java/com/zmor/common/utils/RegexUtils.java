package com.zmor.common.utils;


import com.zmor.common.enums.BaseStateEnumInterface;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 10:37 2018/7/20.
 * @ Description：
 * @ Modified By：
 */
@SuppressWarnings("ALL")
public class RegexUtils {
    private static final Pattern IS_MAIL = Pattern.compile("^.+@.+\\..+$");
    private static final Pattern IS_MOBILE = Pattern.compile("^1[0-9]{10}$");
    private static final Pattern IS_PASSWORD =
            Pattern.compile("^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?![,\\.#%'\\+\\*\\-:;^_`]+$)[,\\.#%'\\+\\*\\-:;^_`0-9A-Za-z]{6,20}$");

    /**
     *
     * 功能描述: 验证邮箱
     *
     * @param: mail
     * @return: boolean
     * @auther: lx
     * @date: 2018/7/20 10:39
     */
    public static boolean mail(String mail){
        return IS_MAIL.matcher(mail).matches();
    }

    /**
     *
     * 功能描述: 验证手机
     *
     * @param: phone
     * @return: boolean
     * @auther: lx
     * @date: 2018/7/20 10:39
     */
    public static boolean phone(String phone){
        return IS_MOBILE.matcher(phone).matches();
    }
    
    /**
     * 验证密码
     * @author wangyang
     * @date 2018/8/30 15:40 
     * @param: phone 
     * @return boolean
    **/
    public static boolean password(String password){
        return IS_PASSWORD.matcher(password).matches();
    }

    /**
     *  判断排序时传参
     * @author wangyang
     * @date 2018/7/27 15:35
     * @param: sqlSort
     * @return boolean
    **/
    public static boolean sqlSort(String sqlSort){
        if(StringUtils.isBlank(sqlSort)) {
            return true;
        }
        sqlSort = sqlSort.trim();
        String desc = "desc";
        String asc = "asc";
        return desc.equalsIgnoreCase(sqlSort) || asc.equalsIgnoreCase(sqlSort);
    }

    /**
     *  判断int状态值是否在枚举范围内  （空值不判断）
     * @author wangyang
     * @date 2018/8/3 15:27
     * @param: state
     * @param: enumClass
     * @return boolean
    **/
    public static boolean enumState(Integer state , Class<? extends BaseStateEnumInterface> enumClass) {
        if(state == null) {
            return true;
        }
        BaseStateEnumInterface[] values = enumClass.getEnumConstants();
        return Arrays.stream(values).anyMatch((o)-> Objects.equals(state,o.getState()));
    }
}
