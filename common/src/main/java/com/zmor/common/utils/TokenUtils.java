package com.zmor.common.utils;

import java.util.UUID;

/**
 * @author wangyang
 * @date 2018/8/30 13:12
 */
public class TokenUtils {

    public static String createToken(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
