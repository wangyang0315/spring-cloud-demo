package com.zmor.common.utils;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.zmor.common.exception.BizException;
import com.zmor.common.exception.BizExceptionEnum;
import org.apache.commons.collections.MapUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

/**
 * bean转化类
 *
 * @author wangyang
 */
public class BeanValidator {

    private static final ValidatorFactory VALIDATOR_FACTORY = Validation.buildDefaultValidatorFactory();

    private static <T> Map<String, String> validate(T t, Class... groups) {
        Validator validator = VALIDATOR_FACTORY.getValidator();
        Set validateResult = validator.validate(t, groups);
        if (validateResult.isEmpty()) {
            return Collections.emptyMap();
        } else {
            Map<String, String> errors = Maps.newLinkedHashMap();
            for (Object aValidateResult : validateResult) {
                ConstraintViolation violation = (ConstraintViolation) aValidateResult;
                errors.put(violation.getPropertyPath().toString(), violation.getMessage());
            }
            return errors;
        }
    }

    private static Map<String, String> validateList(Collection<?> collection) {
        if(collection == null) {
            return Collections.emptyMap();
        }
        Iterator iterator = collection.iterator();
        Map<String, String> errors;

        do {
            if (!iterator.hasNext()) {
                return Collections.emptyMap();
            }
            Object object = iterator.next();
            errors = validate(object);
        } while (errors.isEmpty());

        return errors;
    }

    private static Map<String, String> validateObject(Object first, Object... objects) {
        if (objects != null && objects.length > 0) {
            return validateList(Lists.asList(first, objects));
        } else {
            return validate(first);
        }
    }

    public static void check(Object param) throws BizException {
        Map<String, String> map = BeanValidator.validateObject(param);
        Set<String> errorSet = Sets.newHashSet(map.values());
        if (MapUtils.isNotEmpty(map)) {
            throw new BizException(BizExceptionEnum.PARAM_ERROR.getCode(), Joiner.on(",").join(errorSet));
        }
    }
}
