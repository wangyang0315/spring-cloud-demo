package com.zmor.common.utils;

import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Json字符串与Java对象之间转换的工具类
 *
 * @author muncie [muncie.hoo@qq.com]
 */
public class JsonUtils {
    private static final Gson GSON = new Gson();

    /**
     * Java对象转Json字符串
     *
     * @param object 被转换的Java对象
     * @return 转换后的Json字符串
     */
    public static String toJson(Object object) {
        return GSON.toJson(object);
    }

    /**
     * Json字符串转Java对象
     *
     * @param json 被转换的Json字符串
     * @param <T>  转换后的Java对象类型
     * @return 转换后的Java对象
     */
    public static <T> T fromJsonObject(String json, Class<T> tClass) {
        return GSON.fromJson(json, tClass);
    }

    /**
     * Json字符串转List对象
     *
     * @param json 被转换的Json字符串
     * @param <T> List中的对象类型
     * @return 转换后的List
     */
    public static <T> List<T> fromJsonArray(String json, Class<T> tClass) {
        Type type = new GsonParameterizedType(List.class, new Class[]{tClass});
        return GSON.fromJson(json, type);
    }

    /**
     * Json字符串转Key类型为String的Map对象
     *
     * @param json 被转换的Json字符串
     * @param <T> Map中value的对象类型
     * @return 转换后的Map
     */
    public static <T> Map<String, T> fromJsonMap(String json, Class<T> tClass) {
        Type type = new GsonParameterizedType(Map.class, new Class[]{String.class, tClass});
        return GSON.fromJson(json, type);
    }

    static class GsonParameterizedType implements ParameterizedType {
        private final Class raw;
        private final Type[] args;

        public GsonParameterizedType(Class raw, Type[] args) {
            this.raw = raw;
            this.args = args != null ? args : new Type[0];
        }

        @Override
        public Type[] getActualTypeArguments() {
            return args;
        }

        @Override
        public Type getRawType() {
            return raw;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }
    }

  public static void main(String[] args) {
    System.out.println(toJson("q"));
  }
}
