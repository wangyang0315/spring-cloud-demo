package com.zmor.common.utils;

import com.zmor.common.constants.RedisConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Redis操作工具类.
 *
 * @author muncie [muncie.hoo@qq.com]
 */
@Component
@Slf4j
public class RedisUtils {
    private static StringRedisTemplate template;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @PostConstruct
    public void setStringRedisTemplate() {
        template = this.stringRedisTemplate;
    }

    /**
     * 查询key对应的数据是否存在.
     *
     * @param key 数据的key.
     * @return 如果存在返回true, 如果不存在返回false.
     */
    public static boolean hasKey(String key) {
        return template.hasKey(key);
    }

    /**
     * 设置没有过期时间的数据.
     *
     * @param key   数据key值.
     * @param value 数据.
     */
    public static void set(String key, Object value) {
        template.opsForValue().set(key, JsonUtils.toJson(value));
    }

    /**
     * 设置指定过期时间的数据.
     *
     * @param key     数据key值.
     * @param value   数据.
     * @param timeOut 过期时间(s).
     */
    public static void set(String key, Object value, Integer timeOut) {
        template.opsForValue().set(key, JsonUtils.toJson(value), timeOut, TimeUnit.SECONDS);
    }

    /**
     * 设置默认过期时间的数据.
     *
     * @param key   数据key值.
     * @param value 数据.
     */
    public static void setWithDefaultTimeOut(String key, Object value) {
        template.opsForValue()
                .set(key, JsonUtils.toJson(value), RedisConstants.DEFAULT_TIME_OUT_SEC, TimeUnit.SECONDS);

    }

    /**
     * 如果数据的key不存在, 设置没有过期时间的数据, 并且返回true, 如果数据的key存在, 返回false.
     *
     * @param key   数据的key.
     * @param value 数据.
     * @return 如果数据的key不存在, 返回true, 如果数据的key存在, 返回false.
     */
    public static boolean setIfAbsent(String key, Object value) {
        return template.opsForValue().setIfAbsent(key, JsonUtils.toJson(value));
    }

    /**
     * 如果数据的key不存在, 设置指定过期时间的数据, 并且返回true, 如果数据的key存在, 返回false.
     *
     * @param key     数据key值.
     * @param value   数据.
     * @param timeOut 过期时间(s).
     * @return 如果数据的key不存在, 返回true, 如果数据的key存在, 返回false.
     */
    public static boolean setIfAbsent(String key, Object value, Integer timeOut) {
        if (RedisUtils.hasKey(key)) {
            return false;
        }
        template.opsForValue().set(key, JsonUtils.toJson(value), timeOut, TimeUnit.SECONDS);
        return true;
    }

    /**
     * 根据key值获取数据.
     *
     * @param key 数据key值.
     * @param <T> 数据类型.
     * @return 数据.
     */
    public static <T> T get(String key, Class<T> tClass) {
        String value = template.opsForValue().get(key);
        return JsonUtils.fromJsonObject(value, tClass);
    }

    /**
     * 根据key值获取List对象.
     *
     * @param key 数据key值.
     * @param <T> List中的对象类型.
     * @return List数据.
     */
    public static <T> List<T> getArray(String key, Class<T> tClass) {
        String value = template.opsForValue().get(key);
        return JsonUtils.fromJsonArray(value, tClass);
    }

    /**
     * 根据key值获取Map对象
     *
     * @param key 数据key值.
     * @param <T> Map中value的对象类型.
     * @return Map数据.
     */
    public static <T> Map<String, T> getMap(String key, Class<T> tClass) {
        String value = template.opsForValue().get(key);
        return JsonUtils.fromJsonMap(value, tClass);
    }
    /**
     * 根据key删除
     *
     * @param key 数据key值
     * @return 是否删除成功.
     */
    public static boolean delete(String key) {
        return template.delete(key);
    }
    /**
     * 根据key列表批量删除
     *
     * @param keys 数据key值列表
     * @return 成功删除的数量
     */
    public static Long delete(Collection<String> keys) {
        return template.delete(keys);
    }

}
