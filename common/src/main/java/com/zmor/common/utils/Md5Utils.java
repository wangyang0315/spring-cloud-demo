package com.zmor.common.utils;


import com.zmor.common.exception.BizException;
import com.zmor.common.exception.BizExceptionEnum;
import sun.misc.BASE64Encoder;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 9:07 2018/7/19.
 * @ Description：
 * @ Modified By：
 */
public class Md5Utils {
    public static String createCode(String password, String salt) {
        String str = password + salt;
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new BizException(BizExceptionEnum.MD5_CREATE_ERROR);
        }
        BASE64Encoder base64en = new BASE64Encoder();
        byte[] md5Code;
        md5Code = str.getBytes(StandardCharsets.UTF_8);

        return base64en.encode(md5.digest(md5Code));

    }

}
