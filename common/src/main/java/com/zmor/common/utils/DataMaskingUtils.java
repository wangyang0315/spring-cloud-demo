package com.zmor.common.utils;

/**
 * 数据脱敏
 * @author wangyang
 * @date 2018/8/30 17:30
 */
public class DataMaskingUtils {

    private static final int ID_CARD_LENGTH = 6;
    private static final int BANK_LENGTH = 7;

    /**
     * 身份证脱敏
     * @param idCard 身份证号
     * @return 脱敏后的数据
     */
    public static String idCard(String idCard){
        if (idCard == null || idCard.length() < ID_CARD_LENGTH) {
            return idCard;
        }
        String pre = idCard.substring(0,3);
        String suf = idCard.substring(idCard.length()-2);
        return pre+"***"+suf;
    }

    /**
     * 银行卡脱敏
     *
     * @param bank 银行卡号
     * @return 脱敏后的数据
     */
    public static String bank(String bank){
        if (bank == null || bank.length() < BANK_LENGTH) {
            return bank;
        }
        String pre = bank.substring(0,4);
        String suf = bank.substring(bank.length()-3);
        return pre+"***"+suf;
    }
}
