package com.zmor.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Set;

/**
 * @author wangyang
 * @date 2018/8/15 14:53
 */
@Data
@Accessors(chain = true)
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 5428326912180465599L;
    /**
     * 用户id
     */
    private Long id;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户组id
     */
    private Long groupId;
    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限
     */
    private Set<String> permissions;

}
