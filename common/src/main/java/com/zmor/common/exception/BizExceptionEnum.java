package com.zmor.common.exception;

import lombok.Getter;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 14:32 2018/7/17.
 * @ Description：
 * @ Modified By：
 */
@Getter
public enum BizExceptionEnum {
    /**
     * 业务状态码
     */
    PARAM_ERROR(0,"参数错误"),

    DELETE_ERROR(2,"删除数据失败"),
    MD5_CREATE_ERROR(3, "md5生成失败"),

    ACCOUNT_OR_PWD_ERROR(4,"账号或密码错误"),
    ;
    @SuppressWarnings("CanBeFinal")
    private int code;
    @SuppressWarnings("CanBeFinal")
    private String message;



    @Override
    public String toString() {
        return "BizExceptionEnum{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    BizExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
