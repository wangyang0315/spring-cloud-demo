package com.zmor.common.exception;

import lombok.Getter;

/**
 * @author muncie [muncie.hoo@qq.com]
 */
@Getter
public class BizException extends RuntimeException {
    private static final long serialVersionUID = 1592029986912830655L;
    private final Integer code;



    public BizException(BizExceptionEnum bizExceptionEnum) {
        super(bizExceptionEnum.getMessage());
        this.code = bizExceptionEnum.getCode();
    }

    public BizException(Integer code, String message) {
        super(message);
        this.code = code;
    }

}
