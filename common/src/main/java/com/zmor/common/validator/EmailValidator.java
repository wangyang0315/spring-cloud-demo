package com.zmor.common.validator;


import com.zmor.common.utils.RegexUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author wangyang
 * @date 2018/8/3 14:56
 */
public class EmailValidator implements ConstraintValidator<Email,String> {


    @Override
    public void initialize(Email constraintAnnotation) {

    }

    @Override
    public boolean isValid(String mail, ConstraintValidatorContext constraintValidatorContext) {
        return RegexUtils.mail(mail);
    }
}
