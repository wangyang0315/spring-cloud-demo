package com.zmor.common.validator;


import com.zmor.common.utils.RegexUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author wangyang
 * @date 2018/8/3 14:56
 */
public class PasswordValidator implements ConstraintValidator<Password,String> {


    @Override
    public void initialize(Password constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return RegexUtils.password(value);
    }
}
