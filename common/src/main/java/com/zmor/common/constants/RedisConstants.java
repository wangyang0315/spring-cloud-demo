package com.zmor.common.constants;

/**
 * @author muncie [muncie.hoo@qq.com]
 */
public class RedisConstants {
    // time out start
    /**
     * 默认超时时间.
     */
    public static final Integer DEFAULT_TIME_OUT_SEC = 24 * 60 * 60;
    // key end
}
