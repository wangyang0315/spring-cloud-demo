package com.zmor.common.shiro;

import com.zmor.common.dto.UserDTO;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthenticatedException;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 15:03 2018/7/16.
 * @ Description：
 * @ Modified By：
 */
public class ShiroUtil {
    public static UserDTO getUser() {
        UserDTO userDTO = (UserDTO)SecurityUtils.getSubject().getPrincipal();
        if(userDTO == null){
            throw new UnauthenticatedException("未登录或session超时");
        }
        return userDTO;
    }
}
