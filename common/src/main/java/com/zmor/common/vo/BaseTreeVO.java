package com.zmor.common.vo;

import lombok.Data;

import java.util.List;

/**
 * @author wangyang
 * @date 2018/8/8 13:49
 */
@Data
public abstract class BaseTreeVO<T,ID> {

    protected ID id;

    protected ID parentId;

    /**
     *  前端显示的内容
     */
    protected String label;

    /**
     * 子节点
     */
    protected List<T> children;

}
