package com.zmor.common.support;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author wangyang
 * @date 2018/9/7 15:47
 */
public interface BaseMapper<T> extends Mapper<T>,MySqlMapper<T> {
}
