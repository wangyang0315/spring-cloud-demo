package com.zmor.common.enums;

/**
 * @author wangyang
 * @date 2018/8/3 15:24
 */
public interface BaseStateEnumInterface {
    int getState();
}
