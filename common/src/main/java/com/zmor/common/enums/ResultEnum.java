package com.zmor.common.enums;

import lombok.Getter;

/**
 * @author muncie [muncie.hoo@qq.com]
 */
@SuppressWarnings("CanBeFinal")
@Getter
public enum ResultEnum {
    /**
     * ResultEnum
     */
    SUCCESS(0, "success"),
    UNKNOWN_ERROR(-1, "unknown error"),
    ;

    private Integer code;
    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
