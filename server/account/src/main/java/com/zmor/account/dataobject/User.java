package com.zmor.account.dataobject;

import lombok.Data;
import lombok.experimental.Accessors;
import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * user
 * @author 
 */
@Data
@Accessors(chain = true)
public class User implements Serializable {
    private static final long serialVersionUID = -5073504028212364460L;
    /**
     * 用户id
     */
    @Id
    @KeySql(dialect = IdentityDialect.MYSQL)
    private Long id;

    /**
     * 数据创建时间
     */
    private Date gmtCreate;

    /**
     * 数据最后更新时间
     */
    private Date gmtModified;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户组id
     */
    private Long groupId;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 盐
     */
    private String salt;

    /**
     * 姓名
     */
    private String name;

    /**
     * 实名认证类型(1. 身份证 2. 护照)
     */
    private Integer idType;

    /**
     * 实名认证证件号码
     */
    private String idCard;

    /**
     * 实名认证证件证明1
     */
    @Column(name = "id_card_evidence_1")
    private String idCardEvidence1;

    /**
     * 实名认证证件证明2
     */
    @Column(name = "id_card_evidence_2")
    private String idCardEvidence2;

    /**
     * 绑定的银行卡的银行名称
     */
    private String bank;

    /**
     * 绑定的银行卡的卡号
     */
    private String bankCard;

    /**
     * 绑定的银行卡的账户所在行(开户行)
     */
    private String accountWithBank;

    /**
     * 绑定银行卡的证明1
     */
    @Column(name = "bank_card_evidence_1")
    private String bankCardEvidence1;

    /**
     * 绑定银行卡的证明2
     */
    @Column(name = "bank_card_evidence_2")
    private String bankCardEvidence2;


    /**
     * 推荐人ID
     */
    private Long pid;

    /**
     * 推荐人ID链
     */
    private String pids;
}