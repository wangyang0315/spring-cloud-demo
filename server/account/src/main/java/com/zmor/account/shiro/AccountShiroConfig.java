package com.zmor.account.shiro;

import com.zmor.common.shiro.ShiroConfig;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.RedisCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 15:00 2018/7/16.
 * @ Description：
 * @ Modified By：
 */

@Configuration
public class AccountShiroConfig extends ShiroConfig {


    /**
     * shiro安全管理器
     *
     * @param shiroRealm
     * @return
     */
    @Bean
    @Primary
    public DefaultWebSecurityManager accountSecurityManager(ShiroRealm shiroRealm, RedisCacheManager cacheManager,
                                                     SessionManager sessionManager) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(shiroRealm);
        securityManager.setCacheManager(cacheManager);
        securityManager.setSessionManager(sessionManager);
        return securityManager;
    }

    /**
     * ShiroRealm
     *
     * @return
     */
    @Bean
    public ShiroRealm shiroRealm() {
        return new ShiroRealm();
    }


}
