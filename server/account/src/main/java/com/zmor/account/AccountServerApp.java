package com.zmor.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author wangyang
 * @date 2018/10/11 16:42
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "com.zmor.account.mapper")
public class AccountServerApp {

  public static void main(String[] args) {
      SpringApplication.run(AccountServerApp.class);
  }
}
