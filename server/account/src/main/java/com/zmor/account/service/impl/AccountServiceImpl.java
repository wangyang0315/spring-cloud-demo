package com.zmor.account.service.impl;

import com.zmor.account.bo.RegistParamsBO;
import com.zmor.account.dataobject.User;
import com.zmor.account.mapper.UserMapper;
import com.zmor.account.service.AccountService;
import com.zmor.account.vo.LoginVO;
import com.zmor.common.exception.BizException;
import com.zmor.common.exception.BizExceptionEnum;
import com.zmor.common.shiro.ShiroUtil;
import com.zmor.common.utils.BeanConvertor;
import com.zmor.common.utils.Md5Utils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author wangyang
 * @date 2018/8/30 16:41
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Resource
    private UserMapper userMapper;

    @Override
    public void register(RegistParamsBO params) {

    }

    @Override
    public LoginVO login(String email, String password, Boolean rememberMe) {
        User user;
        user = userMapper.findByEmail(email);
        if (user == null) {
            throw new BizException(BizExceptionEnum.ACCOUNT_OR_PWD_ERROR);
        }
        password = Md5Utils.createCode(password, user.getSalt());
        if (rememberMe == null) {
            rememberMe = false;
        }
        UsernamePasswordToken token = new UsernamePasswordToken(email, password, rememberMe);
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);

        return BeanConvertor.copy(ShiroUtil.getUser(), LoginVO.class);
    }
}
