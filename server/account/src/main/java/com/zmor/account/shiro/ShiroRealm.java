package com.zmor.account.shiro;


import com.google.common.collect.Sets;
import com.zmor.account.dataobject.User;
import com.zmor.account.mapper.UserMapper;
import com.zmor.common.dto.UserDTO;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @Author     ：LX.
 * @ Date       ：Created in 15:01 2018/7/16.
 * @ Description：
 * @ Modified By：
 */
public class ShiroRealm extends AuthorizingRealm {

    @Resource
    private UserMapper userMapper;
  /*  @Resource
    private GroupPermissionMapper groupPermissionMapper;
    @Resource
    private RolePermissionMapper rolePermissionMapper;*/


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        UserDTO userDTO = (UserDTO) principalCollection.getPrimaryPrincipal();

        simpleAuthorizationInfo.addStringPermissions(userDTO.getPermissions());
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
            throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        String email = usernamePasswordToken.getUsername();
        User user = userMapper.findByEmail(email);
        if(user == null) {
            return null;
        }

        Long roleId = user.getRoleId();
        Long groupId = user.getGroupId();
        Set<String> perminsStrSet = Sets.newHashSet();
       /* if(groupId != null){
            List<String> permissionList = groupPermissionMapper.findPermissionListByGroupId(groupId);
            for (String permission : permissionList) {
                if(!StringUtils.isBlank(permission)){
                    perminsStrSet.add(permission);
                }
            }
        }
        if(roleId != null){
            List<String> permissionList = rolePermissionMapper.findPermissionListByRoleId(roleId);
            for (String permission : permissionList) {
                if(!StringUtils.isBlank(permission)){
                    perminsStrSet.add(permission);
                }
            }
        }*/
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId())
                .setEmail(user.getEmail())
                .setRoleId(user.getRoleId())
                .setGroupId(user.getGroupId())
                .setPermissions(perminsStrSet);

        return new SimpleAuthenticationInfo(userDTO, user.getPassword(), getName());
    }
}
