package com.zmor.account.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * @author wangyang
 * @date 2018/8/15 14:53
 */
@Data
@Accessors(chain = true)
public class LoginVO {
    /**
     * 用户id
     */
    private Long id;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户组id
     */
    private Long groupId;
    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限
     */
    private Set<String> permissions;

}
