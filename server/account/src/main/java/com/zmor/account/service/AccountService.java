package com.zmor.account.service;


import com.zmor.account.bo.RegistParamsBO;
import com.zmor.account.vo.LoginVO;

/**
 * @author wangyang
 * @date 2018/8/30 16:40
 */
public interface AccountService {

    /**
     * 注册
     * @param params 参数
     */
    void register(RegistParamsBO params);

    /**
     * 登录
     * @param email 邮箱
     * @param password 密码
     * @param rememberMe 记住我
     * @return 登录信息
     */
    LoginVO login(String email, String password, Boolean rememberMe);
}
