package com.zmor.account.controller;

import com.zmor.account.bo.RegistParamsBO;
import com.zmor.account.service.AccountService;
import com.zmor.account.vo.LoginVO;
import com.zmor.common.exception.BizException;
import com.zmor.common.exception.BizExceptionEnum;
import com.zmor.common.utils.BeanValidator;
import com.zmor.common.utils.RegexUtils;
import com.zmor.common.vo.ResultVO;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 账户信息
 * @author wangyang
 * @date 2018/8/30 10:26
 */
@RestController
public class AccountController {

    @Resource
    private AccountService accountService;


    /**
     * 登录
     *
     * @return com.wisdomlucky.visdoo.server.vo.ResultVO
     * @author wangyang
     * @date 2018/8/15 14:16
     * @param:
     **/
    @PostMapping(value = "/sessions", produces = {"application/json;charset=UTF-8"})
    public ResultVO login(String email, String password, Boolean rememberMe) {
        if (StringUtils.isBlank(email)) {
            throw new BizException(BizExceptionEnum.PARAM_ERROR.getCode(), "邮箱不能为空");
        }
        if (!RegexUtils.mail(email)) {
            throw new BizException(BizExceptionEnum.PARAM_ERROR.getCode(), "邮箱格式不正确");
        }
        if (StringUtils.isBlank(password)) {
            throw new BizException(BizExceptionEnum.PARAM_ERROR.getCode(), "密码不能为空");
        }

        LoginVO loginVO = accountService.login(email, password, rememberMe);

        return ResultVO.success(loginVO);
    }

    /**
     * 退出登录
     *
     * @return com.wisdomlucky.visdoo.server.vo.ResultVO
     * @author wangyang
     * @date 2018/8/21 15:34
     * @param:
     **/
    @DeleteMapping(value = "/sessions", produces = {"application/json;charset=UTF-8"})
    public ResultVO logout() {
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.logout();
            subject.isAuthenticated();
            DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
            sessionManager.setGlobalSessionTimeout(0);
        } catch (Exception ignored) {
        }
        return ResultVO.success();
    }

    /**
     * 注册
     *
     * @return com.wisdomlucky.visdoo.server.vo.ResultVO
     * @author wangyang
     * @date 2018/8/15 10:59
     * @param: params
     **/
    @PostMapping(value = "/register", produces = {"application/json;charset=UTF-8"})
    public ResultVO register(RegistParamsBO params) {
        BeanValidator.check(params);

        accountService.register(params);

        return ResultVO.success();
    }
}
