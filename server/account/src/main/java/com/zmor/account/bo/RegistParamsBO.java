package com.zmor.account.bo;

import com.zmor.common.validator.Email;
import com.zmor.common.validator.Password;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author wangyang
 * @date 2018/8/15 10:48
 */
@Data
public class RegistParamsBO {

    /**
     * 邮箱
     */
    @Email
    private String email;

    /**
     * 密码
     */
    @Password
    private String password;

    /**
     * 验证码
     */
    @NotEmpty(message = "验证码不能为空")
    private String emailCode;

    /**
     * 推荐码
     */
    private String referralCode;
}
