package com.zmor.account.mapper;

import com.zmor.account.dataobject.User;
import com.zmor.common.support.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：wy. @ Date ：Created in 14:29 2018/8/17. @ Description： @ Modified By：
 */
public interface UserMapper extends BaseMapper<User> {


    /**
     * 功能描述: 根据 roleId查用户数
     */
    Integer findAdminCount(Long roleId);

    /**
     * 功能描述:根据email查user
     */
    User findByEmail(String email);

    /**
     * 功能描述: 根据groupId查user
     */
    List<User> findByGroupId(Long groupId);

    /**
     * 功能描述: 根绝roleId查user
     */
    List<User> findByRoleId(Long roleId);

    /**
     * 根据用户组Ids查userId
     */
    List<Long> findUserIdByGroupIds(@Param("groupIds") List<Long> groupIds);


    /**
     * 查询团队的用户Id
     */
    List<Long> findUserIdsForTeam(Long userId);
}
