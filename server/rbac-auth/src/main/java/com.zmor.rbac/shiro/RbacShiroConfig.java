package com.zmor.rbac.shiro;

import com.zmor.common.shiro.ShiroConfig;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangyang
 * @date 2018/10/12 9:21
 */
@Configuration
public class RbacShiroConfig extends ShiroConfig {
}
