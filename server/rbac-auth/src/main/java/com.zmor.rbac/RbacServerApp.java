package com.zmor.rbac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author wangyang
 * @date 2018/10/12 9:14
 */
@SpringBootApplication
@EnableDiscoveryClient
public class RbacServerApp {
  public static void main(String[] args) {
      SpringApplication.run(RbacServerApp.class);
  }
}
